import lombok.SneakyThrows;
import org.example.utils.GoogleQrCodeUtils;
import org.example.utils.HutoolsQrCodeUtils;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @Description test
 * @Author zhouhouliang
 * @Date 2024/2/19 15:59
 */
public class TestQrCode {

    @SneakyThrows
    public static void main(String[] args) {
        HutoolsQrCodeUtils.generateQrCode("https://github.com/", "F:/temp/github1.png");
        GoogleQrCodeUtils.createCodeToFile("https://github.com/",new File("F:/temp"),"github2.png");

    }


}
