package org.example;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @Description 启动类
 * @Author zhou
 * @Date 2024/2/19 15:52
 */
@SpringBootApplication
@Slf4j
public class QrCodeApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(QrCodeApp.class, args);
        ConfigurableEnvironment environment = context.getEnvironment();
        String port = environment.getProperty("server.port", "80");
        log.info("启动成功! http://127.0.0.1:{}", port);
    }

}


