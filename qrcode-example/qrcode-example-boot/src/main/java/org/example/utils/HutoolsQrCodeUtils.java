package org.example.utils;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import lombok.SneakyThrows;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * @Description hutu二维码
 * @Author zhouhouliang
 * @Date 2024/2/19 15:47
 */
public class HutoolsQrCodeUtils {


    @SneakyThrows
    public static void generateQrCode(String content, String file) {
        try (
                FileOutputStream fos = new FileOutputStream(file);
        ) {
            generateQrCode(content, fos);
        }
    }

    public static void generateQrCode(String content, OutputStream os) {
        QrConfig qrConfig = new QrConfig();
        qrConfig.setBackColor(Color.white.getRGB());
        qrConfig.setForeColor(Color.black.getRGB());
        QrCodeUtil.generate(content, qrConfig, "png", os);
    }


}
