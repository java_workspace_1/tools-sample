package org.example.markdown.imgtool.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @Author
 * @Date 2024/4/26 10:11
 */
@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MoveFileReq {

    /**
     * 根目录
     */
    private String rootPath;

    /**
     * markDown 文件所在目录
     */
    private String mdPath;

}
