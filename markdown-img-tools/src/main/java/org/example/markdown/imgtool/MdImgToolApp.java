package org.example.markdown.imgtool;


import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class MdImgToolApp {

    @SneakyThrows
    public static void main(String[] args) {
        SpringApplication.run(MdImgToolApp.class, args);
        String url = "http://127.0.0.1:25030/index.html";
        log.info("启动成功!" + url);
        Runtime.getRuntime().exec("cmd /c start " + url);
    }

}
