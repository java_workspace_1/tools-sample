package org.example.markdown.imgtool.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.example.markdown.imgtool.dto.req.MoveFileReq;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@RequestMapping("/imgFile")
@RestController
public class ImgFileController {

    private static final String DICT_DIR_NAME = "dict";

    private static final ThreadLocal<Integer> threadLocal = new ThreadLocal<>();

    private static final Pattern pattern = Pattern.compile("!\\[.*?\\]\\((.+?)\\)");

    private static final List<String> ignoreFiles = new ArrayList<String>() {{
        add(".git");
        add("/dict");
        add(".apt_generated");
        add(".classpath");
        add(".factorypath");
        add(".project");
        add(".settings");
        add(".springBeans");
        add(".sts4-cache");
        add(".idea");
        add("/nbproject/private/");
        add("/nbbuild/");
        add("/dist");
        add("/nbdist");
        add("/.nb-gradle");
        add("/build");
        add(".vscode");
        add("/target");
        add("/node_modules");
    }};

    @Resource
    private ObjectMapper objectMapper;


    @PostMapping("/moveFile")
    @SneakyThrows
    public String moveFile(@RequestBody MoveFileReq req) {
        log.info("/imgFile/moveFile req:{}", objectMapper.writeValueAsString(req));
        threadLocal.set(0);
        try {
            String rootPath = req.getRootPath();
            String mdPath = req.getMdPath();
            Assert.isTrue(StringUtils.isNoneBlank(rootPath), "dictPath 不能为空");
            Assert.isTrue(StringUtils.isNoneBlank(mdPath), "handlePath 不能为空");
            rootPath = rootPath.replace("\\", "/");
            rootPath = cutLast(rootPath, "/");
            mdPath = mdPath.replace("\\", "/");
            mdPath = cutLast(mdPath, "/");
            Assert.isTrue(mdPath.startsWith(rootPath), "mdPath 必须在 rootPath下");

            /* ------ 创建图片目录 ------ */
            String dictPath = rootPath + "/" + DICT_DIR_NAME;
            File dictFile = new File(dictPath);
            if (dictFile.exists() && dictFile.isDirectory()) {
                log.info("文件夹{}已存在,不重复创建", dictFile.getAbsolutePath());
            } else if (!dictFile.exists()) {
                boolean mkdirs = dictFile.mkdirs();
                log.info("文件夹{}创建{}", dictFile.getAbsolutePath(), mkdirs ? "成功" : "失败");
                if (!mkdirs) {
                    return "文件夹创建失败";
                }
            } else {
                return "请检查dict目录:" + dictPath;
            }

            /* ------ 遍历目录,寻找可以copy的图片 ------ */
            List<File> mdFiles = new ArrayList<>();
            walkFile(new File(mdPath), mdFiles);
            log.info("找到md文件{}个", mdFiles.size());

            /* ------ ------ */
            for (File mdFile : mdFiles) {
                handleOneFile(mdFile, dictPath);
            }
            return "ok";
        } finally {
            threadLocal.remove();
        }
    }


    private String cutLast(String originText, String cut) {
        if (originText.endsWith(cut)) {
            int i = originText.lastIndexOf(cut);
            return originText.substring(0, i);
        }
        return originText;
    }


    /**
     * @param mdFile
     * @param dictPath
     * @return
     */
    @SneakyThrows
    private String handleOneFile(File mdFile, String dictPath) {
        log.info("开始处理文件{}", mdFile.getAbsolutePath());
        StringBuilder newFileText = new StringBuilder();
        try (
                FileInputStream fis = new FileInputStream(mdFile);
                InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);
        ) {
            String line = null;
            while ((line = br.readLine()) != null) {
                // 读取图片
                Matcher matcher = pattern.matcher(line);
                while (matcher.find()) {
                    String imgLink = matcher.group();
                    int count = matcher.groupCount();
                    if (count == 1) {
                        String url = matcher.group(1);
                        // 检查url是否已经在dictPath
                        boolean rightUrl = url.replace("\\", "/").startsWith(dictPath.replace("\\", "/")) || url.startsWith("./") || url.startsWith("../");
                        if (rightUrl) {
                            continue;
                        }
                        // 复制图片
                        String newUrl = copyFile(new File(url), dictPath);
                        // 和markDown文件保持相对路径
                        newUrl = findRelationPath(mdFile, new File(newUrl));

                        String newImgLink = imgLink.replace(url, newUrl);
                        if (StringUtils.isNotBlank(newImgLink)) {
                            line = line.replace(imgLink, newImgLink);
                        }
                    }
                }
                newFileText.append(line)
                        .append("\n");
            }
        }
        /* ------ 写入文件 ------ */
        try (
                FileOutputStream fos = new FileOutputStream(mdFile);
                OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
        ) {
            osw.write(newFileText.toString());
            osw.flush();
        }
        log.info("完成处理文件{}", mdFile.getAbsolutePath());
        return newFileText.toString();
    }


    /**
     * 转换相对路径
     * E:/所有笔记/web-front-notes/00_收集文档/xx.md  00_收集文档/xx.md
     * E:/所有笔记/web-front-notes/xx.md  /xx.md
     * E:/所有笔记/web-front-notes/dict/aa.png
     *
     * @param markdownFile
     * @param imgFile
     * @return ../dict/aa.png
     */
    private String findRelationPath(File markdownFile, File imgFile) {
        File imgDirFile = imgFile.getParentFile();
        String imgDirName = imgDirFile.getName();
        String rootPath = imgDirFile.getParentFile().getPath().replace("\\", "/");

        String mdFilePath = markdownFile.getAbsolutePath().replace("\\", "/");

        Assert.isTrue(mdFilePath.startsWith(rootPath), "md文件路径非法!!!! markdownFile:" + markdownFile.getAbsolutePath() + ",imgFile:" + markdownFile.getAbsolutePath());

        String replace = mdFilePath.replace(rootPath, "");
        if (replace.startsWith("/")) {
            replace = replace.substring(1);
        }
        int level = StringUtils.countMatches(replace, "/");
        String relativePath = "";
        if (level == 0) {
            relativePath = "./";
        } else {
            for (int i = 0; i < level; i++) {
                relativePath += "../";
            }
        }

        return relativePath + imgDirName + "/" + imgFile.getName();
    }


    @SneakyThrows
    private String copyFile(File file, String savePath) {
        if (file.exists() && file.isFile()) {
            String fileName = file.getName();
            int i = fileName.lastIndexOf(".");
            String extension = "";
            if (i > 0) {
                extension = fileName.substring(i);
            }

            String newFilePath = savePath + "/" + file.getName();
            File targetFile = new File(newFilePath);
            if (targetFile.exists()) {
                newFilePath = savePath + "/" + numGenerator() + extension;
            }
            try (
                    FileInputStream fis = new FileInputStream(file);
                    BufferedInputStream bis = new BufferedInputStream(fis);
                    FileOutputStream fos = new FileOutputStream(newFilePath);
                    BufferedOutputStream bos = new BufferedOutputStream(fos);
            ) {
                IOUtils.copy(bis, bos);
                bos.flush();
            }
            log.info("复制文件成功!file:{},newFile:{}", file.getAbsolutePath(), newFilePath);
            return newFilePath;
        } else {
            log.error("复制文件失败!文件不存在或非文件.file:{}", file.getAbsolutePath());
            return null;
        }
    }


    private void walkFile(File file, List<File> mdFiles) {
        String absolutePath = file.getAbsolutePath().replace("\\", "/");
        for (String ignoreFile : ignoreFiles) {
            if (absolutePath.contains(ignoreFile)) {
                return;
            }
        }

        if (file.isFile() && file.getName().endsWith(".md")) {
            mdFiles.add(file);
            return;
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null && files.length > 0) {
                for (File childFile : files) {
                    walkFile(childFile, mdFiles);
                }
            }
        }
    }


    private String numGenerator() {
        Integer index = threadLocal.get();
        index++;
        if (index > 999) {
            index = 0;
            threadLocal.set(index);
        }

        String indexString = index.toString();
        int length = indexString.length();
        for (int i = (3 - length); i > 0; i--) {
            indexString = "0" + indexString;
        }

        return DateFormatUtils.format(new Date(), "yyyyMMdd_HHmmss_SSS_") + indexString;
    }


}
