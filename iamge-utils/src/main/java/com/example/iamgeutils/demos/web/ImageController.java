package com.example.iamgeutils.demos.web;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.ifok.image.image4j.codec.ico.ICOEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLEncoder;
import java.util.Collections;

@Slf4j
@RequestMapping("/image")
@RestController
public class ImageController {

    @SneakyThrows
    @PostMapping("/toIco")
    public String toIco(MultipartFile file, String savePath, @RequestParam(required = false) String fileName, @RequestParam(required = false) Integer icoSize) {
        String icoFileName = null;
        String originalFilename = file.getOriginalFilename();
        if (StringUtils.hasLength(fileName)) {
            icoFileName = fileName.concat(".ico");
        } else {
            if (originalFilename.contains(".")) {
                int i = originalFilename.lastIndexOf(".");
                originalFilename = originalFilename.substring(0, i);
            }
            icoFileName = originalFilename.concat(".ico");
        }

        if (!StringUtils.hasLength(savePath)) {
            savePath = "E:/";
        } else if (!savePath.endsWith("/")) {
            savePath += "/";
        }
        String icoPath = savePath + icoFileName;
        try (InputStream is = file.getInputStream();
             BufferedInputStream bis = new BufferedInputStream(is);
             FileOutputStream fos = new FileOutputStream(icoPath);
             BufferedOutputStream bos = new BufferedOutputStream(fos);
        ) {
            BufferedImage img = ImageIO.read(bis);

            // 设置大小分辨率
            icoSize = (icoSize != null && icoSize > 0) ? icoSize : 64;
            Image icoImage = img.getScaledInstance(icoSize, icoSize, Image.SCALE_DEFAULT);
            BufferedImage bufferedImage = new BufferedImage(icoSize, icoSize, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics = bufferedImage.createGraphics();
            graphics.drawImage(icoImage, 0, 0, null);
            graphics.dispose();

            ICOEncoder.write(Collections.singletonList(bufferedImage), bos);
            bos.flush();
            log.info("转换文件成功!{}  ->  {}", originalFilename, icoPath);
        }
        return icoPath;
    }

    /**
     * 设置响应结果
     *
     * @param response    响应结果对象
     * @param rawFileName 文件名
     * @throws UnsupportedEncodingException 不支持编码异常
     */
    private static String setDownloadResp(HttpServletResponse response, String rawFileName) throws UnsupportedEncodingException {
        if (rawFileName.contains(".")) {
            int i = rawFileName.lastIndexOf(".");
            rawFileName = rawFileName.substring(0, i);
        }
        String icoFileName = rawFileName.concat(".ico");

        //设置内容类型
        response.setContentType("application/vnd.vnd.ms-excel");
        //设置编码格式
        response.setCharacterEncoding("utf-8");
        //设置导出文件名称（避免乱码）
        String encodeFileName = URLEncoder.encode(icoFileName, "UTF-8");
        // 设置响应头
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + encodeFileName);
        return icoFileName;
    }

}
