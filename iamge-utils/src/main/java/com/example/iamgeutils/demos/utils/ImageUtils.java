package com.example.iamgeutils.demos.utils;

import lombok.SneakyThrows;
import net.ifok.image.image4j.codec.ico.ICOEncoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Collections;

/**
 * @Description 图片处理工具类
 * @Author zhouhouliang
 * @Date 2024/1/29 15:54
 */
public class ImageUtils {

    @SneakyThrows
    public static void parse2Icon(String inputPath, String outputPath) {

        // 把需要关闭的流放在 try（  里 ）{。。。
        try (
                BufferedInputStream bis = new BufferedInputStream(new FileInputStream(inputPath));
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outputPath));
        ) {
            // 读取 图片
            BufferedImage img = ImageIO.read(bis);
            

            // 写出 图片
//            ICOEncoder.write(Arrays.asList(img), bos);
            ICOEncoder.write(Collections.singletonList(img), bos);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        File file = new File("D:\\软件快捷方式/Ico转换.bat - 快捷方式");
    }


}
