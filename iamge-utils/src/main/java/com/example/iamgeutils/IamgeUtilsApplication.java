package com.example.iamgeutils;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class IamgeUtilsApplication {

    @SneakyThrows
    public static void main(String[] args) {
        SpringApplication.run(IamgeUtilsApplication.class, args);
        String url = "http://127.0.0.1:25020/index.html";
        log.info("启动成功!" + url);
        Runtime.getRuntime().exec("cmd /c start " + url);
    }

}
