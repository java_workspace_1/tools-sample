package com.example.groovysample.demos.utils;

import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import groovy.util.GroovyScriptEngine;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;

import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.util.Map;

/**
 * @Description Groovy脚本工具类
 * @Author zhouhouliang
 * @Date 2024/1/9 10:11
 */
public class GroovyUtils {

    /**
     * 方式一: 脚本
     * 执行一段脚本
     *
     * @param script
     * @param <T>
     * @return
     */
    public static <T> T execShellScript(String script) {
        GroovyShell groovyShell = new GroovyShell();
        return (T) groovyShell.evaluate(script);
    }

    /**
     * 方式一: 脚本
     * 执行带参数的脚本方法
     *
     * @param script
     * @param params
     * @param <T>
     * @return
     */
    public static <T> T execShellScript(String script, Map<String, Object> params) {
        Binding binding = new Binding();
        params.forEach((s, o) -> {
            binding.setProperty(s, o);
        });
        GroovyShell groovyShell = new GroovyShell(binding);
        return (T) groovyShell.evaluate(script);
    }


    /**
     * 方式二: 用类加载器加载Groovy类,然后反射调用
     *
     * @param script
     * @param <T>
     * @return
     */
    @SneakyThrows
    public static <T> T execGroovyClass(String script, String methodName, Object... args) {
        Class[] argsTypes = new Class[args.length];
        for (int i = 0; i < args.length; i++) {
            argsTypes[i] = args[i].getClass();
        }
        GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
        Class aClass = groovyClassLoader.parseClass(script);
        Method method = aClass.getMethod(methodName, argsTypes);
        Object instance = aClass.newInstance();
        return (T) method.invoke(instance, args);
    }


    /**
     * 使用GroovyScriptEngine脚本引擎加载Groovy脚本
     */
    @SneakyThrows
    public static void loadGroovyByEngine() {
        ClassPathResource classPathResource = new ClassPathResource("/scripts/");
        URL[] urls = new URL[1];
        urls[0]=classPathResource.getURL();
        GroovyScriptEngine groovyScriptEngine = new GroovyScriptEngine(urls);

        Binding binding = new Binding();
        binding.setProperty("words","劳资要吃鱼");

        groovyScriptEngine.run("Cat.groovy", binding);
    }

}
