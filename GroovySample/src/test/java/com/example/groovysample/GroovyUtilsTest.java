package com.example.groovysample;

import com.example.groovysample.demos.utils.GroovyUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Objects;

/**
 * @Description 工具类测试
 * @Author zhouhouliang
 * @Date 2024/1/9 10:15
 */
@SpringBootTest
public class GroovyUtilsTest {

    @Test
    public void testExecShellScript() {
        int result = GroovyUtils.execShellScript("println('呱呱呱');\n return 1");
        System.out.println(result);
    }


    @Test
    public void testExecShellScript2() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("words", "呱呱呱");

        Object o = GroovyUtils.execShellScript("def sayWords(words){\n" +
                "    println(\"你说:\" + words);\n" +
                "}\n" +
                "\n" +
                "sayWords(words);",params);
        System.out.println(o);
    }


    @Test
    public void exec() {
        String script = "class Dog {\n" +
                "\n" +
                "    String doSth(String cmd) {\n" +
                "        println(\"执行:\" + cmd);\n" +
                "        return \"我坐好了\"\n" +
                "    }\n" +
                "\n" +
                "    String bite() {\n" +
                "        return \"我咬死你\"\n" +
                "    }\n" +
                "\n" +
                "}";
        Object o = GroovyUtils.execGroovyClass(script, "doSth", "坐下");
        System.out.println(o);
        o = GroovyUtils.execGroovyClass(script, "bite");
        System.out.println(o);
    }


    @Test
    public void test() {
        GroovyUtils.loadGroovyByEngine();
    }
}
